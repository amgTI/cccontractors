<?php get_header(); ?>

<!-- Taxonomy -->

<?php $term = $wp_query->get_queried_object(); ?>
<section class="section_category">
    <div class="section_category__title">
        <div data-aos="fade-up">
            <h1>
                <?php $currentlang = pll_current_language(); if($currentlang=="en"): ?>
                    <?php echo $term->name; ?>
                    <span><?php _e('services','contractors'); ?></span>
                <?php elseif($currentlang=="es"): ?>
                    <?php
                        $title = get_field('title_home_es_categoria', $term);
                        $title_array = explode('/', $title);
                        $first_word = $title_array[0];
                        $second_word = $title_array[1];
                        $third_word = $title_array[2];
                    ?>
                    <?php echo $first_word; ?> 
                    <span><?php echo $second_word; ?></span>
                <?php endif ?>
            </h1>
            <p><?php echo $term->description; ?></p>
        </div>
    </div>
    <div class="section_category__info">
        <?php
            $args = array( 
                'post_type' => 'service', 
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'categoria',
                        'field' => 'term_id',
                        'terms' => $term->term_id,
                    )
                )
            );
            $wp_query = new WP_Query($args); ?>
        <?php if($wp_query->have_posts()) : $a = 0 ?>
        <?php while ($wp_query->have_posts()) : $wp_query->the_post(); $a++ ?>

        <div class="section_category_info_services modal_open_service tab_info_text" id="service-<?php echo $a; ?>">

            <div class="section_category_info_text ">
                <div data-aos="fade-right" class="container">
                    <div>
                        <h3><?php the_title(); ?></h3>
                        <div class="description">
                            <div class="description_desktop">
                                <?php the_content(); ?>
                            </div>
                            <div class="description_mobile">
                                <?php the_excerpt_max_charlength(50); ?>
                            </div>
                        </div>
                    </div>
                    <div class="content_btn">
                        <span class="btn-contact"><?php _e('Request Service','contractors'); ?></span>
                    </div>
                </div>

            </div>
            <div class="section_category_info_image">
                <span class="icon-icon-close closeModal"></span>
                <picture>
                    <source media="(min-width: 768px)" srcset="<?php the_post_thumbnail_url(); ?>">
                    <img src="<?php the_field('image_product')?>" alt="<?php the_title(); ?>">
                </picture>

            </div>
        </div>
        <?php endwhile; ?>
        <?php wp_reset_query(); ?>
        <?php endif; ?>
    </div>
    <p id="btnShowMore" class="btn_more_jobs"><?php the_field('label_categorys_general', 'options'); ?></p>
    <div class="section_category__services">
        <h2><?php _e('Our services','contractors'); ?></h2>
        <div class="content_list">
            <ul>
                <?php
                $args = array( 
                    'post_type' => 'service', 
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'tax_query' => array( 
                        array(
                            'taxonomy' => 'categoria',
                            'field' => 'term_id',
                            'terms' => $term->term_id,
                        )
                    )
                );
                $wp_query = new WP_Query($args); ?>
                <?php if($wp_query->have_posts()) : $b = 0 ?>
                <?php while ($wp_query->have_posts()) : $wp_query->the_post(); $b++ ?>
                <li class="btn-tab" data-id="service-<?php echo $b; ?>"><?php the_title(); ?></li>
                <?php endwhile; ?>
                <?php wp_reset_query(); ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</section>
<div id="modalCategory" class="category_modal hide">
    <!-- Contentido modal from Java Script -->
</div>


<?php get_footer(); ?>