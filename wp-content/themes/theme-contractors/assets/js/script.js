/* Variables */
var bgOverlay = document.querySelector(".bg-overlay");
var panelContact = document.querySelector(".contact_content");

function activePanelContact(activar) {
    if (activar) {
        panelContact.classList.add("active");
        bgOverlay.classList.add("active");
    } else {
        panelContact.classList.remove('active');
        bgOverlay.classList.remove("active");
    }
}

/* close button header */
var closeBtn = document.querySelectorAll('.closePanel');
closeBtn.forEach(e => {
    //e.href = "#";
    e.addEventListener("click", function() {
        activePanelContact(false);
        document.querySelector('.header').classList.remove('active');
    });
});


/* Open panel contact */
var arrayContact = document.querySelectorAll(".btn-contact");
arrayContact.forEach(e => {
    //e.href = "#";
    e.addEventListener("click", function(event) {
        event.preventDefault();
        document.querySelector('.contact').classList.add('active');
        activePanelContact(true);

    });
});

/* Tabs category*/
function tabs(btnsTabs, contentTabs, clase) {
    btnsTabs.forEach(btn => {
        btn.addEventListener('click', function() {
            var attribute = btn.getAttribute("data-id");
            contentTabs.forEach(function(element, index, array) {
                if (element.getAttribute('id') === attribute) {
                    element.classList.add(clase);

                    [...btnsTabs].forEach(function(element, index, array) {
                        element.classList.remove(clase);
                    });

                    btn.classList.add("active");
                } else {
                    element.classList.remove(clase);
                }
            });
        });
    });
}
let btnServices = document.querySelectorAll('.btn-tab');
let contentService = document.querySelectorAll('.tab_info_text');

var services = document.querySelector(".section_category");
if (services != null) {
    contentService[0].classList.add("active");
    btnServices[0].classList.add("active");
}

tabs(btnServices, contentService, "active");

/* swiper objetives y testimonies */
const swiper2 = new Swiper(".objetivesSwiper", {
    slidesPerView: 1,
    simulateTouch: false,
    pagination: {
        el: ".pagination_objetives",
    },
    breakpoints: {
        640: {
            slidesPerView: 1,
            spaceBetween: 20,
        },
        768: {
            slidesPerView: 1,
            spaceBetween: 40,
        },
        1024: {
            slidesPerView: 3,
            spaceBetween: 40,
        },
    },
});

const swiper = new Swiper(".sliderTestimonies", {
    slidesPerView: 1,
    speed: 1350,
    loop: true,
    spaceBetween: 30,

    navigation: {
        nextEl: ".icon-arrow-next",
        prevEl: ".icon-arrow-prev",
    },
    autoplay: {
        delay: 1500,
        disableOnInteraction: false,
    },
    pagination: {
        el: ".pagination_testimonies",
    },
});

const btnShowMore = document.querySelector("#btnShowMore");
// if (btnShowMore) {
//     let currentIndex = 1;
//     var arrayElements = document.querySelectorAll(".item_test");

//     if (arrayElements.length > 6) {
//         currentIndex = 6;
//     }
//     /* Habilita primeros 6 elementos */
//     for (let i = 0; i < 6; i++) {
//         arrayElements[i].style.display = "block";
//     }
//     /* Boton Mostrar Mas */
//     btnShowMore.addEventListener("click", (e) => {
//         for (let i = currentIndex; i < currentIndex + 6; i++) {
//             if (arrayElements[i]) {
//                 arrayElements[i].style.display = "block";
//             }
//         }
//         currentIndex += 6;

//         if (currentIndex >= arrayElements.length) {
//             event.target.style.display = "none";
//         }
//     });
// }

let imageBgs = document.querySelectorAll('.js-bg');

/* swiper projects */
let arraySwiperProjects = document.querySelectorAll(".projects_item");

for (let i = 1; i <= arraySwiperProjects.length; i++) {

    let navigationNext = `.projects-next-${i}`;
    let navigationPrev = `.projects-prev-${i}`;

    let objectSwiper = {
        loop: true,
        slidesPerView: "auto",
        autoplay: {
            delay: 1500,
        },
        speed: 1500,
        spaceBetween: 20,
        navigation: {
            nextEl: navigationNext,
            prevEl: navigationPrev,
        },
        centeredSlides: false,
        breakpoints: {
            // when window width is <= 499px

            // when window width is <= 999px
            768: {
                slidesPerView: 2,
                spaceBetweenSlides: 20
            },

            1024: {
                slidesPerView: 5,
                spaceBetweenSlides: 40,
            }
        }
    }

    var imgs = arraySwiperProjects[i - 1].querySelectorAll(".img_slide");
    if (imgs.length > 4) {
        new Swiper(`.swiperProjects-${i}`, objectSwiper)
    }

}
/* medidas para cambiar imagen en desktop y mobile */
function medidas() {
    for (var i = 0; i < imageBgs.length; i++) {
        if (window.innerWidth > 767) {
            imageBgs[i].style.backgroundImage = 'url(' + imageBgs[i].getAttribute('data-img-desktop') + ')';
        }
        if (window.innerWidth <= 767) {
            imageBgs[i].style.backgroundImage = 'url(' + imageBgs[i].getAttribute('data-img-mobile') + ')';
        }
    }
}
medidas();
window.addEventListener('resize', medidas, true);

/* Open menu mobile*/
let openMenu = document.querySelector(".open-menu");
openMenu.addEventListener("click", function() {
    document.querySelector('.header').classList.add('active');
    bgOverlay.classList.add('active');

});

/*Modal category mobile */
(function main() {
    if (window.matchMedia("(max-width: 1025px)").matches) {
        let boxes = Array.from(document.getElementsByClassName("modal_open_service"));
        let modal = document.getElementById("modalCategory");

        let bodyModal;

        boxes.forEach(function(box) {

            box.addEventListener("click", function() {
                modal.innerHTML = "";
                bodyModal = document.createElement("div");
                bodyModal.classList.add("modal-body");
                bodyModal.innerHTML = box.innerHTML;
                modal.appendChild(bodyModal);

                modal.classList.remove("hide");
                bgOverlay.classList.add("active");

                let close = modal.querySelector(".closeModal");
                // console.log(icon);
                // close = document.createElement("div");
                // close.classList("close");
                close.addEventListener("click", function() {
                        modal.classList.add("hide");
                        bgOverlay.classList.remove("active");
                    })
                    // modal.appendChild(close);


            });
        });
        bgOverlay.addEventListener("click", function() {
            modal.classList.add("hide");
        });
    }
})();