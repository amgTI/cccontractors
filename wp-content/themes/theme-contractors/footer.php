    </main>
    <footer class="footer" style="background-image: url(<?php the_field('banner_footer', 'options') ?>)">
        <section class="footer_container">
            <article data-aos="fade-right" class="footer_container__form">
                <h3>
                    <?php $currentlang = pll_current_language(); if($currentlang=="en"): ?>
                    <?php the_field('title_contact', 'options') ?>
                    <?php elseif($currentlang=="es"): ?>
                    <?php the_field('title_contact_es', 'options') ?>
                    <?php endif ?>
                </h3>
                <p>
                    <?php $currentlang = pll_current_language(); if($currentlang=="en"): ?>
                    <?php the_field('text_contact', 'options') ?>
                    <?php elseif($currentlang=="es"): ?>
                    <?php the_field('text_contact_es', 'options') ?>
                    <?php endif ?>
                </p>
                <?php // get_template_part('templates/content-form'); ?>
            </article>

            <article data-aos="fade-up" class="footer_container__menu">
                <h3>
                    <?php $currentlang = pll_current_language(); if($currentlang=="en"): ?>
                    <?php the_field('title_footer', 'options') ?>
                    <?php elseif($currentlang=="es"): ?>
                    <?php the_field('title_footer_es', 'options') ?>
                    <?php endif ?>
                </h3>

                <?php wp_nav_menu( [
                    'theme_location' => 'header-menu',
                        'container' => false,
                        'menu_class' => 'footer_links'
                    ]);
                ?>

                <div class="footer_contact_links">
                    <ul>
                        <li>
                            <a href="mailto:<?php the_field( 'email','options'); ?>"><?php the_field( 'email','options'); ?>
                                <span class="icon-email-2"></span>
                            </a>
                        </li>
                        <li>
                            <a class=" "
                                href="tel:<?php the_field( 'phone','options'); ?>"><?php the_field( 'phone','options'); ?>
                                <span class="icon-telephone"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </article>

        </section>
    </footer>
    <?php wp_footer(); ?>
    </body>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <script>
let aos = AOS.init({
    // Global settings:
    disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
    startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
    initClassName: 'aos-init', // class applied after initialization
    animatedClassName: 'aos-animate', // class applied on animation
    useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
    disableMutationObserver: false, // disables automatic mutations' detections (advanced)
    debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
    throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)
    // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
    offset: 120, // offset (in px) from the original trigger point
    delay: 0, // values from 0 to 3000, with step 50ms    
    duration: 1000, // values from 0 to 3000, with step 50ms
    easing: 'ease', // default easing for AOS animations
    once: false, // whether animation should happen only once - while scrolling down
    mirror: false, // whether elements should animate out while scrolling past them
    anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation
    disable: function() {
        var maxWidth = 960;
        return window.innerWidth < maxWidth;
    }
});
    </script>

    </html>