<?php
  if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
      'page_title'    => 'Configuración General',
      'menu_title'    => 'Configuraciones',
      'menu_slug'     => 'theme-general-settings',
      'capability'    => 'edit_posts',
      'redirect'      => false,
      'position'      => '2.1',
      'icon_url'      => 'dashicons-admin-settings',
    ));
}
function register_my_menus() {
    register_nav_menus(
      array(
		    'header-menu' => __( 'Header Menu' ), 
        'social-menu' => __( 'Social Menu' )
     )
    );
}
add_action( 'init', 'register_my_menus' ); 

// add theme suppor post thumbnails
add_theme_support( 'post-thumbnails' );

// Support Titulos
add_theme_support( 'title-tag' );

// Remover Editor
function remove_editor() {
  remove_post_type_support('page', 'editor');
}
add_action('admin_init', 'remove_editor');

function scripts(){
  wp_register_style('style', get_stylesheet_directory_uri() . '/sass/styles.css', [], 1, 'all'); 
  wp_enqueue_style('style');
  
}
add_action('wp_enqueue_scripts', 'scripts');
add_action('get_header', 'my_filter_head');
 
function add_this_script_footer(){
   //Swipper Js
   wp_register_script('swipper-js', get_stylesheet_directory_uri() . '/assets/js/swiper.js');
   wp_enqueue_script('swipper-js');
   wp_register_script('aos', get_stylesheet_directory_uri() . '/assets/js/swiper.js');
  

   wp_register_script('fslight', get_stylesheet_directory_uri() . '/assets/js/fslight.js');
   wp_enqueue_script('fslight');
   //uncompiled Script
  wp_register_script('script-js', get_stylesheet_directory_uri() . '/assets/js/script.js');
   wp_enqueue_script('script-js');
}
add_action('wp_footer', 'add_this_script_footer');

function my_filter_head() {
   remove_action('wp_head', '_admin_bar_bump_cb');
} 
load_theme_textdomain('contractors', get_template_directory() . '/languages');

add_filter( 'wpcf7_autop_or_not', '__return_false' );
// social media header
function wp_custom_social_media($content = null) {
  ob_start(); ?>
<?php if( have_rows('social_networks', 'options') ): ?>
<?php while( have_rows('social_networks', 'options') ): the_row(); ?>
<?php 
        $name_select_sub_field = (get_sub_field_object('type_social_networks','options'));
        $name_sub_field = get_sub_field('type_social_networks','options');
        $label_select = ($name_select_sub_field['choices'][$name_sub_field]);
    ?>
<?php if( get_sub_field('type_social_networks', 'options') ) { ?>
<li>
    <a class="icon-<?php the_sub_field( 'type_social_networks','options' ); ?>"
        href="<?php the_sub_field('url_social_networks', 'options'); ?>" target="_blank" title="Coming Soon"> </a>
</li>
<?php } ?>
<?php endwhile; ?>
<?php endif;
  $content = ob_get_contents();
  ob_end_clean();
  return $content;  
}
add_shortcode('social_media', 'wp_custom_social_media');



//* Shortcode Testimonies Slider
function wp_testimonies_section($content = null) {
	ob_start(); ?>
<section class="testimonies js-bg" data-img-desktop="<?php the_field('banner_comments','options'); ?>"
    data-img-mobile="<?php the_field('banner_comments_mobile','options');?>">
    <div class="swiper-container sliderTestimonies">
        <ul class="swiper-wrapper">
            <?php if( have_rows('list_comments', 'options') ): $b = 0  ?>
            <?php while( have_rows('list_comments', 'options') ): the_row(); $b++ ?>
            <li class="swiper-slide">
                <div class="testimonies_content">
                    <span class="icon-quot"></span>
                    <?php $currentlang = pll_current_language(); if($currentlang=="en"): ?>
                    <p><?php the_sub_field('text_list_comments', 'options') ?> </p>
                    <strong><?php the_sub_field('name_list_comments', 'options') ?></strong>
                    <?php elseif($currentlang=="es"): ?>
                    <p><?php the_sub_field('text_list_comments_es', 'options') ?> </p>
                    <strong><?php the_sub_field('name_list_comments_es', 'options') ?></strong>
                    <?php endif ?>
                </div>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
        </ul>
        <div class="arrows">
            <span class="arrow icon-arrow2 icon-arrow-prev"></span>
            <span class="arrow icon-arrow2 icon-arrow-next"></span>
        </div>
    </div>
    <div class="swiper-pagination pagination_testimonies"></div>
</section>
<?php
	$content = ob_get_contents();
	ob_end_clean();
	return $content;  
}
add_shortcode('testimonies_section', 'wp_testimonies_section');

// About 
function wp_about_section($content = null) {
	ob_start(); ?>
<?php $currentlang = pll_current_language(); if($currentlang=="en"): ?>
<h2><?php the_field('title_about_general','options'); ?></h2>
<?php the_field('text_about_general','options'); ?>
<?php elseif($currentlang=="es"): ?>
<h2><?php the_field('title_about_general_es','options'); ?></h2>
<?php the_field('text_about_general_es','options'); ?>
<?php endif ?>
<?php
	$content = ob_get_contents();
	ob_end_clean();
	return $content;  
}
add_shortcode('about_section', 'wp_about_section');

// Objectives 
function wp_objective_section($content = null) {
	ob_start(); ?>
<section class="objectives_general">
    <div data-aos="fade-up">
        <?php $currentlang = pll_current_language(); if($currentlang=="en"): ?>
        <h2><?php the_field('title_objectives','options'); ?></h2>
        <p><?php the_field('text_objectives','options'); ?></p>
        <?php elseif($currentlang=="es"): ?>
        <h2><?php the_field('title_objectives_es','options'); ?></h2>
        <p><?php the_field('text_objectives_es','options'); ?></p>
        <?php endif ?>
    </div>
    <div class="objectives_container">
        <div class="swiper-container objetivesSwiper">
            <div class="swiper-wrapper">
                <?php if( have_rows('list_objectives', 'options') ): $b = 0  ?>
                <?php while( have_rows('list_objectives', 'options') ): the_row(); $b++ ?>
                <div class="swiper-slide">
                    <div data-aos="fade-up" class="objectives_slider_item">
                        <figure>
                            <img src="<?php the_sub_field('image_list_objectives','options'); ?>" alt="">
                        </figure>
                        <p>
                            <?php $currentlang = pll_current_language(); if($currentlang=="en"): ?>
                            <?php the_sub_field('text_list_objectives','options'); ?>
                            <?php elseif($currentlang=="es"): ?>
                            <?php the_sub_field('text_list_objectives_es','options'); ?>
                            <?php endif ?>
                        </p>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
        <div class="swiper-pagination pagination_objetives"></div>
    </div>
</section>
<?php
	$content = ob_get_contents();
	ob_end_clean();
	return $content;  
}
add_shortcode('objective_section', 'wp_objective_section');

/*
 *-----------------------------------
Select Services
-----------------------------------* 
*/
function add_listServices_to_CF7 ( $tag, $unused ) {
  if ( $tag['name'] != 'list_services' ){
        return $tag;
  }
  $args = get_posts(array(
      'post_type' => 'service',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'update_post_meta_cache' => false,
      'update_post_term_cache' => false,
  ));

  foreach ( $args as & $arg ) {
      $tag['raw_values'][] = $arg->post_title;
      $tag['values'][] = $arg->post_title;
  }
  return $tag;
}
add_filter( 'wpcf7_form_tag', 'add_listServices_to_CF7', 10, 2);

function the_excerpt_max_charlength($charlength) {
	$excerpt = get_the_excerpt();
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
		echo '...';
	} else {
		echo $excerpt;
	}
}