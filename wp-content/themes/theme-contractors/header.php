<!DOCTYPE html>
<html lang="<?php bloginfo('language'); ?>">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, , user-scalable=no">
    <meta charset="<?php bloginfo('charset'); ?>">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>">
    <?php wp_head(); ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-0SKYYGGSNG"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0SKYYGGSNG');
</script>

</head>

<body <?php body_class(); ?>>
    <div class="menu_multilanguage">
        <ul class=""><?php pll_the_languages(); ?></ul>
    </div>
    <div class="bg-overlay closePanel"></div>
    <figure class="header_logo">
        <img src="<?php the_field('logo','options')?>" alt="">
    </figure>
    <ul class="social_media_mobile">
        <?php echo do_shortcode('[social_media]'); ?>
    </ul>
    <div class="open-menu">
        <span class="icon-hamburguer-menu"></span>
    </div>
    <header class="header "
        style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/banner-header.jpg" );>
        <span class="icon-icon-close closePanel"></span>
        <figure class="header_logo">
            <img src="<?php the_field('logo','options')?>" alt="">
        </figure>
        <?php wp_nav_menu( array(
            'theme_location' => 'header-menu',
                'container' => false,
                'menu_class' => 'navbar-list'
            ));
        ?>
        <div class="header_socials">
            <ul>
                <?php echo do_shortcode('[social_media]'); ?>
                <li><a href="mailto:<?php the_field( 'email','options'); ?>" class="icon-email"></a></li>
                <li><a href="tel:<?php the_field( 'phone','options'); ?>" class="icon-phone"></a></li>
            </ul>
            <a class="text_contact"
                href="mailto:<?php the_field( 'email','options'); ?>"><?php the_field( 'email','options'); ?></a>
        </div>
    </header>

    <div class="contact">
        <div class="contact_content">
            <div class="contact_content_container">
                <div class="btn_container">
                    <span class="icon-icon-close closePanel"></span>
                </div>
                <div class="contact_content__title">
                    <?php $currentlang = pll_current_language(); if($currentlang=="en"): ?>
                    <h3><?php the_field('title_contact_two','options'); ?></h3>
                    <p><?php the_field('text_contact_two','options'); ?></p>
                    <?php elseif($currentlang=="es"): ?>
                    <h3><?php the_field('title_contact_two_es','options'); ?></h3>
                    <p><?php the_field('text_contact_two_es','options'); ?></p>
                    <?php endif ?>
                </div>
                <?php // get_template_part( 'templates/content-2-form'); ?>
            </div>
        </div>
    </div>

    <!-- MAIN SITE -->
    <main class="main_site">