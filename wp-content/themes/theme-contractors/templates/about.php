<?php
/* Template Name: Template - About*/
get_header(); ?>
<!-- About -->
<section class="about_info">
    <div class="about_info_image">
        <picture>
            <source media="(min-width: 768px)" srcset="<?php the_field('image_about')?>">
            <img src="<?php the_field('image_about_mobile')?>" alt="About">
        </picture>
    </div>
    <div class="about_info_text">
        <div data-aos="fade-left">
            <?php echo do_shortcode('[about_section]'); ?>
        </div>
    </div>
</section>

<!-- Objectives -->
<?php echo do_shortcode('[objective_section]'); ?>

<!-- Testimonies -->
<?php echo do_shortcode('[testimonies_section]'); ?>

<!-- Questions -->
<section class="about_tips">
    <div data-aos="fade-up" class="content_title">
        <h2><?php the_field('title_questions') ?></h2>
        <p><?php the_field('text_questions') ?></p>
    </div>
    <ul>
        <?php if( have_rows('list_questions') ): $a = 0  ?>
        <?php while( have_rows('list_questions') ): the_row(); $a++ ?>
        <li>
            <h4><?php the_sub_field('title_list_questions') ?></h4>
            <p><?php the_sub_field('text_list_questions') ?></p>
        </li>
        <?php endwhile; ?>
        <?php endif; ?>
    </ul>
</section>

<?php get_footer(); ?>