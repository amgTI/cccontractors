<?php $currentlang = pll_current_language(); if($currentlang=="en"): ?>
	<?php
		$post_object = get_field('form_contact','options');
			if( $post_object ): 
				$post = $post_object;
				setup_postdata( $post ); 
				$cf7_id = $post->ID;
				echo do_shortcode( '[contact-form-7 id="'.$cf7_id.'" ]' ); 
				wp_reset_query();
		endif;
	?>
<?php elseif($currentlang=="es"): ?>
	<?php
		$post_object = get_field('form_contact_es','options');
			if( $post_object ): 
				$post = $post_object;
				setup_postdata( $post ); 
				$cf7_id = $post->ID;
				echo do_shortcode( '[contact-form-7 id="'.$cf7_id.'" ]' ); 
				wp_reset_query();
		endif;
	?>
<?php endif ?>