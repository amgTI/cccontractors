<?php
/* Template Name: Template - Home*/
get_header(); ?>


<!-- Banner -->
<section class="banner_categorys">
    <?php
        $terms = get_terms("categoria",array('hide_empty' => false));
        $count = count($terms);
        if ( $count > 0 ){ ?>
    <?php
                $a = 1;
                foreach ( $terms as $term ) { 
                    $term_link = get_term_link( $term );
            ?>

    <a href="<?php echo $term_link; ?>" class="banner_categorys__content js-bg"
        data-img-desktop="<?php the_field('image_categoria', $term); ?>"
        data-img-mobile="<?php the_field('image_categoria_mobile', $term);?>">
        <div class="bg_image"></div>
        <div class="text_bg">
            <h1>
                <?php $currentlang = pll_current_language(); if($currentlang=="en"): ?>
                    <?php echo $term->name; ?>
                    <span><?php _e('services','contractors'); ?></span>
                <?php elseif($currentlang=="es"): ?>
                    <?php
                        $title = get_field('title_home_es_categoria', $term);
                        $title_array = explode('/', $title);
                        $first_word = $title_array[0];
                        $second_word = $title_array[1];
                        $third_word = $title_array[2];
                    ?>
                    <?php echo $first_word; ?> 
                    <span><?php echo $second_word; ?></span>
                <?php endif ?>
            </h1>

            <p><?php _e('More information about the service','contractors'); ?></p>
        </div>
    </a>

    <?php
                 $a++;
                } ?>
    <?php
        }
    ?>
</section>

<!-- About -->
<section class="home_company">
    <div class="home_company__container">
        <div class="home_company__container--info">
            <div data-aos="fade-right" class="content">
                <?php echo do_shortcode('[about_section]'); ?>
                <?php $currentlang = pll_current_language(); if($currentlang=="en"):
                $href =  esc_url( get_page_link( 134 ) );
            elseif($currentlang=="es"):
                    $href =  esc_url( get_page_link( 304 ) );
            endif ?>
                <div>
                    <a class="btn_red" href="<?php echo $href ?>"><?php the_field('label_about_home'); ?></a>
                </div>

            </div>

        </div>
        <div class="home_company__container--image">
            <img src="<?php the_field('image_about_home'); ?>" alt="About">
        </div>
    </div>
</section>

<!-- Objectives -->
<?php echo do_shortcode('[objective_section]'); ?>

<!-- Testimonies -->
<?php echo do_shortcode('[testimonies_section]'); ?>

<!-- Gallery -->
<section class="gallery">
    <div data-aos="fade-up">
        <h2><?php the_field('title_gallery_home'); ?></h2>
        <p><?php the_field('text_gallery_home'); ?> </p>
    </div>
    <ul>
        <?php 
        $images = get_field('list_gallery_home');
        if( $images ): ?>
        <?php foreach( $images as $image ): ?>
        <a href="<?php echo esc_url($image['url']); ?>" data-fslightbox="gallery" class="gallery-item item_test">
            <div class="eye-wrapper">

            </div>
            <p class="eye"><i class="icon-icon-eye"></i></p>
            <img src="<?php echo esc_url($image['url']); ?>" alt="Image" title="Image  Description" />
        </a>
        <?php endforeach; ?>
        <?php endif; ?>
    </ul>
    <p id="btnShowMore" class="bt_more_jobs"><?php the_field('label_gallery_home'); ?></p>
</section>



<?php get_footer(); ?>