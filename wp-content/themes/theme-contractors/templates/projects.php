<?php
/* Template Name: Template - Projects*/
get_header(); ?>
<section class="projects">
    <div class="projects_title">
        <div data-aos="fade-up">
            <h1><?php the_title(); ?></h1>
            <p><?php the_field('text_projects'); ?></p>
        </div>
    </div>
    <div class="projects_container">
        <?php
            $args = array( 
                'post_type' => 'project', 
                'post_status' => 'publish',
                'posts_per_page' => -1,
            );
            $wp_query = new WP_Query($args); ?>
        <?php if($wp_query->have_posts()) : $a = 0 ?>
        <?php while ($wp_query->have_posts()) : $wp_query->the_post(); $a++ ?>
        <div class="projects_item ">
            <div data-aos="fade-right">
                <h3><?php the_title(); ?></h3>
                <p><?php the_field('client_project'); ?></p>
            </div>
            <div class="projects_item_gallery">
                <div class="swiper-container swiperProjects-<?php echo $a; ?>">
                    <div class="swiper-wrapper">
                        <?php $images = get_field('list_project');
                                if( $images ): ?>
                        <?php foreach( $images as $image ): ?>
                        <div class="swiper-slide">
                            <a href="<?php echo esc_url($image['url']); ?>" data-fslightbox="gallery"
                                class="gallery-item">
                                <img class="img_slide" src="<?php echo esc_url($image['url']); ?>" alt="Image"
                                    title="Image Description" />
                            </a>
                        </div>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="swiper-button-next projects-next-<?php echo $a; ?> "></div>
                <div class="swiper-button-prev projects-prev-<?php echo $a; ?>"></div>
            </div>
        </div>
        <?php endwhile; ?>
        <?php wp_reset_query(); ?>
        <?php endif; ?>

    </div>

    <p id="btnShowMore" class="bt_more_jobs"><?php the_field('label_projects'); ?></p>

</section>




<?php get_footer(); ?>